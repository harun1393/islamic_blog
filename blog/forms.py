from django import forms 
from blog.models import Post


class AddPostForm(forms.ModelForm):
    class Meta:
        model = Post 
        # fields = '__all__'
        exclude = ('author',)
        widgets = {
            'title': forms.TextInput(attrs={'class': 'form-control'}),
            'description': forms.Textarea(attrs={'class': 'form-control'})
        }

