from django.urls import include, path
from .views import *


urlpatterns = [
    path('add-post', add_new_post, name='add-post'),
    path('post_detail/<int:pk>', post_detail, name='post_detail'),
    path('posts_of_category/<int:pk>', posts_of_category, name='posts_of category'),
    path('total_author/', total_author, name='total_author'),
    path('posts_of_author/<int:pk>', posts_of_author, name='posts_of_author'),
]