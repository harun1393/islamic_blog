from django.shortcuts import render
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404
from rest_framework.authtoken.models import Token


from .models import Category, Post, Profile
from .forms import AddPostForm


def add_new_post(request):
	categories = Category.objects.all()
	token, created = Token.objects.get_or_create(user=request.user)
	context = {'categories': categories, 'token': str(token.key)}
	return render(request, 'blog/add-new-post.html', context)


# post_detail_page_view
def post_detail(request, pk):
	post = get_object_or_404(Post, pk=pk)
	first = Post.objects.filter(is_draft=False).first()
	last = Post.objects.filter(is_draft=False).last()
	ctx = {'post': post, 'first': first, 'last': last}
	return render(request, 'blog/post_detail.html', ctx)


# posts_of_Category_page_view
def posts_of_category(request, pk):
	category_obj = get_object_or_404(Category, pk=pk)
	post = Post.objects.filter(category=category_obj, is_draft=False)
	ctx = {'category': category_obj, 'post': post}
	return render(request, 'blog/posts_of_category.html', ctx)


# total_author_page_view
def total_author(request):
	authors = User.objects.all()
	return render(request, 'blog/total_author.html', {'authors': authors})


# posts_of_author_page_view
def posts_of_author(request, pk):
	author = get_object_or_404(User, pk=pk)
	post = Post.objects.filter(author=author, is_draft=False)
	ctx = {'author': author, 'post': post}
	return render(request, 'blog/posts_of_author.html', ctx)	


