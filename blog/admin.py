from django.contrib import admin

from .models import Tag, Category, Post, Profile


class TagAdmin(admin.ModelAdmin):
	list_display = ('id', 'name')


class CategoryAdmin(admin.ModelAdmin):
	list_display = ('id', 'name')


class PostAdmin(admin.ModelAdmin):
	list_display = ('id', 'is_draft', 'title')
	list_editable = ['is_draft']


admin.site.register(Tag, TagAdmin),
admin.site.register(Category, CategoryAdmin),
admin.site.register(Post, PostAdmin),
admin.site.register(Profile),