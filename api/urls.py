from django.urls import include, path
from . import views 


urlpatterns = [
    path('blog/add-new-post/', views.AddPostView.as_view()),
]
