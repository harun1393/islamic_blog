from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .serializers import AddPostSerializer


class AddPostView(APIView):
    def post(self, request):
        print("main data",request.data)
        serializer = AddPostSerializer(data=request.data)
        if serializer.is_valid():
            print(serializer.validated_data)
            serializer.save(author=request.user)
            return Response({'status': True}, status=status.HTTP_201_CREATED)
        print(serializer.errors)
        return Response({'status': serializer.errors}, status=status.HTTP_400_BAD_REQUEST)
