from rest_framework import serializers
from django.utils.safestring import mark_safe
from blog.models import Post, Category


class AddPostSerializer(serializers.ModelSerializer):
    description = serializers.CharField()

    class Meta:
        model = Post 
        fields = ('title', 'description', 'category', 'is_draft')
        #exclude = ('date', 'author')

    def create(self, validated_data):
        return Post.objects.create(**validated_data)

    def get_description(self, instance):
        print("in get desc", instance)
        return mark_safe(instance.description)
