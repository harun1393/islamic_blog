from django.urls import include, path
from .views import *

app_name = 'dashboard'

urlpatterns = [
    path('', dashboard, name='home'),
    path('post/list', authors_post_list, name='post-list'),
]