from django.shortcuts import render
from blog.models import Post 


def dashboard(request):
    return render(request, 'dashboard/dashboard.html')

def authors_post_list(request):
    posts = Post.objects.authors_post(author=request.user)
    context = {'posts': posts}
    return render(request, 'dashboard/authors_post_list.html', context)
