from django.shortcuts import render
from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404

from blog.models import Category, Post


# home_page_view
def home(request):
	Categorys = Category.objects.all()
	latest_post = Post.objects.latest_post()
	ctx = {'categories': Categorys, 'latest_post': latest_post}
	return render(request, 'blog/home.html', ctx)


